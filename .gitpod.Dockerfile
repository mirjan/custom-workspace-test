FROM gitpod/workspace-full

RUN git clone https://github.com/LiaScript/CodiLIA.git /home/gitpod/.codi/
RUN sudo mkdir -p /ide && sudo chown gitpod:gitpod /ide
RUN echo "{\"entrypoint\": \"/ide/startup.sh\", \"readinessProbe\": { \"type\": \"http\", \"http\": { \"path\": \"\" }}}" > /ide/supervisor-ide-config.json
RUN echo "cd /home/gitpod/.codi/deployments && docker-compose up -d" > /ide/startup.sh && chmod +x /ide/startup.sh
